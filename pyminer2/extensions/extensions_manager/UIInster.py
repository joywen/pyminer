'''
这是加载UI控件的插件类。
插件导入时参数由json中的config设置项所决定。

config:是一个字典,但是现在传入的参数还远远不够。
以这个文件的json配置为例，有以下要求：
"file":声明了控件类的入口位置。一般就是在main.py之中。
position:插件插入位置。有两种选项：‘new_dock_window’和‘new_toolbar’
config:设置属性。
config.message:插件的设置信息，可以为空。
config.name:插件的名称
config.side:插件插入窗口时的位置（当position=new_dock_window时有效），有left\right\top\bottom四个选项
config.text:插件的文字。会显示在dockwidget或者工具栏上
{
    "file":"main.py",
    "widget":"WidgetTest",
    "position":"new_dock_window",
    "config":{
        "message":"no",
        "name":"codeedit",
        "side": "right",
        "text": "编辑器"
    }
}
'''
from typing import TYPE_CHECKING
from pyminer2.extensions.extensionlib.pmext import PluginInterface

if TYPE_CHECKING:
    from PyQt5.QtWidgets import QWidget


class BaseInserter:
    @classmethod
    def insert(cls, widget: 'QWidget', config=None):
        print("BaseInserter.insert({widget},{config})".format(widget=repr(widget), config=repr(config)))
        raise NotImplementedError


class NewTab(BaseInserter):
    pass


class InsertIntoTab(BaseInserter):
    pass


class NewToolBar(BaseInserter):
    @classmethod
    def insert(cls, widget: 'QWidget', config=None):
        name = config['name']
        text = config['text']
        PluginInterface.add_tool_bar(name, widget, text)


class NewDockWindow(BaseInserter):
    @classmethod
    def insert(cls, widget: 'QWidget', config=None):
        dock_name = config['name']
        text = config['text']
        side = config['side']
        PluginInterface.add_docked_widget(dock_name=dock_name, widget=widget, text=text, side=side)


class AppendToolbar(BaseInserter):
    @classmethod
    def insert(cls, widget: 'QWidget', config=None):
        button_name = config['name']
        text = config['text']
        icon_path = config.get('icon')
        toolbar_name = config['toolbar']
        PluginInterface.get_toolbar(toolbar_name).add_widget(button_name,widget)


class NewDialog(BaseInserter):
    @classmethod
    def insert(cls, widget: 'QWidget', config=None):
        # dock_name = config['name']
        # text = config['text']
        # side = config['side']
        # PluginInterface.add_docked_widget(dock_name=dock_name, widget=widget, text=text, side=side)
        PluginInterface.get_main_window().dialogs[config['name']] = widget


ui_inserters = {
    'new_dock_window': NewDockWindow,
    'new_toolbar': NewToolBar,
    'append_to_toolbar': AppendToolbar,
    'new_dialog': NewDialog
}
